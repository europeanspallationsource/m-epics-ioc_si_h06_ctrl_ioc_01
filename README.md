# IOC

EPICS module with "business logic" of the IOC.
IOC Factory allows sets up the IOC to use separate modules for all the devices that the IOC controls (PLC, MKS, PS, BCM, etc.)
But often the IOC itself needs to have some "business logic", for example some records and sequence that uses all of those devices and runs some sort of algorithm (e.g. start-up sequence).
This should be placed in this module.

* This is a module specific to a particular IOC.

If you need to create a module for a new IOC:

1. Create a copy of this module
2. Name accordingly
3. Modify database, sequences, etc.
