#!/bin/bash

# VLAN config commands for HV and HVAC networks
# Network interface to use (e.g. eth0, eth1, ...)
interface=eth1

# HV system
vlanid2=2
interfacevlanid2=${interface}.${vlanid2}
ip link add link $interface name $interfacevlanid2 type vlan id $vlanid2
ip addr add 172.17.0.69/24 brd 172.17.0.255 dev $interfacevlanid2
ip link set dev $interfacevlanid2 up

# HVAC system 
vlanid3=3
interfacevlanid3=${interface}.${vlanid3}
ip link add link $interface name $interfacevlanid3 type vlan id $vlanid3
ip addr add 172.17.15.10/24 brd 172.17.15.255 dev $interfacevlanid3
ip link set dev $interfacevlanid3 up

